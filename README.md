## Hello container

This is a test repo to test container builds for Modio.  Don't expect anything
more than hello world and a well implemented .gitlab-ci.yml


Targets:

This shows how to use our templates from the [Modio CI repo](https://gitlab.com/ModioAB/CI) for what we consider a "normal" CI workflow for a project.


General workflow:

1. check
1. prepare
1. build
1. test
1. publish


## Check stage

1. Check code for syntax correctness (fast, early pre-flight check)

## Prepare stage

1. Prepare stage before building, meant for gathering other artifacts, downloading data-files, packages and more that will be needed in the build stage.  
   Commonly used when you need a special environment for something that's needed in the build stage.

## Build stage

1. Main build stage for artifacts and later stages
1. Build and publish temporary containers


## Test stage

1. Run test-cases against built libraries / binaries
1. Run various extra linting tools
1. Run Integration tests against the temp containers
1. Rebase against main branch, run tests on every non-fixup commit [*]   
1. Rebase against main branch, run check on every non-fixup commit [*]
1. Rebase against main branch, run lints on every non-fixup commit [*]

[*] Only on branches and merge-requests, not on tags or release-builds.

## Deploy stage

1. Pull the temporary container and re-publish it as final name
1. Upload other artifacts for release criterium

